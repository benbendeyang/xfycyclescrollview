//
//  SubViewController.swift
//  XFYCycleScrollView_Example
//
//  Created by 🐑 on 2019/3/13.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import XFYCycleScrollView

class SubViewController: UIViewController {

    @IBOutlet weak var cycleScrollView: CycleScrollView!
    @IBOutlet weak var bannerView: BannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        cycleScrollView.delegate = self
    }
    
    @IBAction func clickSetView(_ sender: Any) {
        let count = Int(arc4random()%10) + 1
        print("创建\(count)个视图")
        var views = [UIView]()
        for i in 0..<count {
            let view = UILabel()
            view.text = i.description
            view.textAlignment = .center
            view.font = UIFont.systemFont(ofSize: 20)
            view.backgroundColor = UIColor.red
            views.append(view)
        }
        cycleScrollView.setupViews(views: views)
        
        bannerView.setupBanner(urls: [
            "http://b-ssl.duitang.com/uploads/item/201510/04/20151004080802_nTJYz.jpeg",
            "http://b-ssl.duitang.com/uploads/item/201612/10/20161210110010_cwsxT.jpeg",
            "http://g.hiphotos.baidu.com/image/pic/item/4e4a20a4462309f7e6a224cf780e0cf3d6cad61a.jpg"])
        bannerView.didTouchAtIndex = { index in
            print("bannerView点击:", index)
        }
        bannerView.didScrollToIndex = { index in
            print("bannerView滚动:", index)
        }
    }
    
    deinit {
        print("销毁")
    }
}

extension SubViewController: CycleScrollViewDelegate {
    
    func cycleScrollView(cycleScrollView: CycleScrollView, touchAt index: NSInteger) {
        print("点击了 \(index)")
    }
    func cycleScrollView(cycleScrollView: CycleScrollView, scrollTo index: NSInteger) {
        print("滚动到了 \(index)")
    }
//    func cycleScrollViewWillBeginDragging(cycleScrollView: CycleScrollView) {
//        print("开始拖拽")
//    }
//    func cycleScrollViewDidEndDragging(cycleScrollView: CycleScrollView) {
//        print("结束拖拽")
//    }
//    func cycleScrollViewDidScroll(cycleScrollView: CycleScrollView) {
//        print("正在滚动")
//    }
//    func cycleScrollViewWillBeginDecelerating(cycleScrollView: CycleScrollView) {
//        print("准备减速")
//    }
//    func cycleScrollViewDidEndDecelerating(cycleScrollView: CycleScrollView) {
//        print("结束减速")
//    }
}
