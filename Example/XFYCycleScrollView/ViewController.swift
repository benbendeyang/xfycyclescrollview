//
//  ViewController.swift
//  XFYCycleScrollView
//
//  Created by leonazhu on 03/12/2019.
//  Copyright (c) 2019 leonazhu. All rights reserved.
//

import UIKit
import XFYCycleScrollView

class ViewController: UIViewController {

    @IBOutlet weak var cycleScrollView: CycleScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

private extension ViewController {
    
    func initView() {
        let count = Int(arc4random()%10) + 1
        print("创建\(count)个视图")
        var views = [UIView]()
        for i in 0..<count {
            let view = UILabel()
            view.text = i.description
            view.textAlignment = .center
            view.font = UIFont.systemFont(ofSize: 20)
            view.backgroundColor = UIColor.red
            views.append(view)
        }
        cycleScrollView.setupViews(views: views)
        
        let cycleScrollView1 = CycleScrollView(frame: CGRect(x: 20, y: 300, width: 100, height: 100))
        view.addSubview(cycleScrollView1)
        let view11 = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 200, height: 100)))
        view11.backgroundColor = .red
        let view12 = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 100, height: 50)))
        view12.backgroundColor = .yellow
        let view13 = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 50, height: 50)))
        view13.backgroundColor = .gray
        cycleScrollView1.setupViews(views: [view11, view12, view13])
        
        let bannerView = BannerView(frame: CGRect(x: 140, y: 300, width: 100, height: 100))
        view.addSubview(bannerView)
        bannerView.setupBanner(urls: [
            "http://b-ssl.duitang.com/uploads/item/201510/04/20151004080802_nTJYz.jpeg",
            "http://b-ssl.duitang.com/uploads/item/201612/10/20161210110010_cwsxT.jpeg",
            "http://g.hiphotos.baidu.com/image/pic/item/4e4a20a4462309f7e6a224cf780e0cf3d6cad61a.jpg"])
        
        let cycleScrollView3 = CycleScrollView(frame: CGRect(x: 260, y: 300, width: 100, height: 100))
        view.addSubview(cycleScrollView3)
        let view31 = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 200, height: 100)))
        view31.backgroundColor = .red
        cycleScrollView3.setupViews(views: [view31])
    }
}
