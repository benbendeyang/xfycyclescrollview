# XFYCycleScrollView

[![CI Status](https://img.shields.io/travis/leonazhu/XFYCycleScrollView.svg?style=flat)](https://travis-ci.org/leonazhu/XFYCycleScrollView)
[![Version](https://img.shields.io/cocoapods/v/XFYCycleScrollView.svg?style=flat)](https://cocoapods.org/pods/XFYCycleScrollView)
[![License](https://img.shields.io/cocoapods/l/XFYCycleScrollView.svg?style=flat)](https://cocoapods.org/pods/XFYCycleScrollView)
[![Platform](https://img.shields.io/cocoapods/p/XFYCycleScrollView.svg?style=flat)](https://cocoapods.org/pods/XFYCycleScrollView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XFYCycleScrollView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'XFYCycleScrollView'
```

## Author

leonazhu, 412038671@qq.com

## License

XFYCycleScrollView is available under the MIT license. See the LICENSE file for more info.
