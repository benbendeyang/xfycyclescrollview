//
//  BannerView.swift
//  Pods-XFYCycleScrollView_Example
//
//  Created by 🐑 on 2019/3/14.
//

import UIKit
import Kingfisher

open class BannerView: CycleScrollView {

    public var didTouchAtIndex: ((_ index: Int) -> Void)?
    public var didScrollToIndex: ((_ index: Int) -> Void)?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
}

// MARK: - 公共方法
public extension BannerView {
    
    /// 设置Banner
    func setupBanner(urls: [String], placeholder: UIImage? = nil, contentMode: UIView.ContentMode = .scaleAspectFill) {
        var views = [UIView]()
        urls.forEach { url in
            let imageView = UIImageView()
            imageView.contentMode = contentMode
            imageView.layer.masksToBounds = true
            imageView.kf.setImage(with: URL(string: url), placeholder: placeholder)
            views.append(imageView)
        }
        setupViews(views: views)
    }
}

// MARK: - 私有方法
private extension BannerView {
    
    /// 初始化
    func initView() {
        delegate = self
    }
}

// MARK: - 协议
extension BannerView: CycleScrollViewDelegate {
    
    public func cycleScrollView(cycleScrollView: CycleScrollView, touchAt index: NSInteger) {
        didTouchAtIndex?(index)
    }
    public func cycleScrollView(cycleScrollView: CycleScrollView, scrollTo index: NSInteger) {
        didScrollToIndex?(index)
    }
}
