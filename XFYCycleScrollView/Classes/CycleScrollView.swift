//
//  CycleScrollView.swift
//  Pods-XFYCycleScrollView_Example
//
//  Created by 🐑 on 2019/3/12.
//
//  轮播图
//  思路:创建n+2的滚动图，前后留白，当滚动到第一页或最后一页时，重置位置

import UIKit

@objc public protocol CycleScrollViewDelegate {
    /// 开始拖拽
    @objc optional func cycleScrollViewWillBeginDragging(cycleScrollView: CycleScrollView)
    /// 结束拖拽
    @objc optional func cycleScrollViewDidEndDragging(cycleScrollView: CycleScrollView)
    /// 正在滚动
    @objc optional func cycleScrollViewDidScroll(cycleScrollView: CycleScrollView)
    /// 准备减速
    @objc optional func cycleScrollViewWillBeginDecelerating(cycleScrollView: CycleScrollView)
    /// 停止滚动
    @objc optional func cycleScrollViewDidEndDecelerating(cycleScrollView: CycleScrollView)
    /// 滚动到哪一个index
    @objc optional func cycleScrollView(cycleScrollView: CycleScrollView, scrollTo index: Int)
    /// 点击了哪一个index
    @objc optional func cycleScrollView(cycleScrollView: CycleScrollView, touchAt index: Int)
}


open class CycleScrollView: UIView {
    
    // MARK: - 属性
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.bounces = false
        scrollView.delegate = self
        return scrollView
    }()
    private var contentViews: [UIView] = [] {
        didSet {
            isInitComplete = false
        }
    }
    
    /// 初始化完成
    private var isInitComplete: Bool = false
    
    /// 当前显示的索引
    public private(set) var currentIndex: Int = 0 {
        didSet {
            guard oldValue != currentIndex else { return }
            delegate?.cycleScrollView?(cycleScrollView: self, scrollTo: currentIndex)
        }
    }
    
    /// 动画时间
    var animateDuration: TimeInterval = 0.25
    /// 定时器时间间隔
    public var timeInterval: TimeInterval = 2
    /// 定时器
    private var timer: Timer?
    
    /// 代理
    public weak var delegate: CycleScrollViewDelegate?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        scrollView.frame = bounds
        scrollViewSetNeedsLayout()
    }
    
    open override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        if newWindow != nil {
            addTimer()
        } else {
            removeTimer()
        }
    }
}

// MARK: - 公共方法
public extension CycleScrollView {
    
    /// 创建 以及 重置 显示数组
    func setupViews(views: [UIView]) {
        removeTimer()
        // 清空
        scrollView.subviews.forEach { subview in
            subview.removeFromSuperview()
        }
        contentViews = views
        // 添加
        contentViews.forEach { view in
            scrollView.addSubview(view)
        }
        // 布局
        setNeedsLayout()
        // 开启定时器
        addTimer()
    }
    
    /// 手动选择显示对象 可选择动画
    func scroll(to index: Int, animated: Bool) {
        if contentViews.count > 1, isInitComplete, index >= 0, index < contentViews.count {
            removeTimer()
            scrollView.setContentOffset(CGPoint(x: CGFloat(index + 1) * bounds.width, y: 0), animated: animated)
            addTimer()
        }
    }
    
    /// 下一页
    func next() {
        guard contentViews.count > 1 else { return }
        // 取整，矫正滚动错位
        let truncIndex: Int = Int(scrollView.contentOffset.x/bounds.width)
        scrollView.setContentOffset(CGPoint(x: CGFloat(truncIndex + 1) * bounds.width, y: 0), animated: true)
    }
}

// MARK: - 私有方法
private extension CycleScrollView {
    
    /// 初始化
    func initView() {
        addSubview(scrollView)
        let tap = UITapGestureRecognizer(target: self, action: #selector(CycleScrollView.clickTap(tap:)))
        addGestureRecognizer(tap)
    }
    
    /// 手势点击
    @objc func clickTap(tap: UITapGestureRecognizer) {
        if contentViews.count > 0 {
            delegate?.cycleScrollView?(cycleScrollView: self, touchAt: currentIndex)
        }
    }
    
    /// 布局scrollView
    func scrollViewSetNeedsLayout() {
        let width = bounds.width
        let height = bounds.height
        let count = contentViews.count
        setSubviewFrame()
        if count > 1 {
            scrollView.contentSize = CGSize(width: CGFloat(count + 2) * width, height: height)
            scrollView.contentOffset = CGPoint(x: width, y: 0)
        } else {
            scrollView.contentSize = CGSize(width: width, height: height)
        }
        if !isInitComplete {
            isInitComplete = true
            scroll(to: 0, animated: false)
        }
        synchronization()
    }
    
    /// 布局子视图
    func setSubviewFrame() {
        let width = bounds.width
        let height = bounds.height
        let count = contentViews.count
        for i in 0..<count {
            let subview = contentViews[i]
            if count > 1 {
                subview.frame = CGRect(x: CGFloat(i + 1) * width, y: 0, width: width, height: height)
            } else {
                subview.frame = CGRect(x: 0, y: 0, width: width, height: height)
            }
        }
    }
    
    /// 计算位置
    func synchronization() {
        let count = contentViews.count
        guard count > 1 else { return }
        let width = bounds.width
        let height = bounds.height
        let offX = scrollView.contentOffset.x
        if offX < width {
            // 第一页空白显示
            contentViews.last?.frame = CGRect(x: 0, y: 0, width: width, height: height)
        } else if offX > CGFloat(count) * width {
            // 最后一页空白
            contentViews.first?.frame = CGRect(x: CGFloat(count + 1) * width, y: 0, width: width, height: height)
        } else {
            setSubviewFrame()
        }
        if offX < 0.1 {
            scrollView.contentOffset = CGPoint(x: CGFloat(count) * width, y: 0)
        } else if offX > (scrollView.contentSize.width - width - 0.1) {
            scrollView.contentOffset = CGPoint(x: width, y: 0)
        }
        let index = Int(scrollView.contentOffset.x / width - 0.5)
        if index >= 0, index < contentViews.count {
            currentIndex = index
        }
    }
    
    // MARK: 定时器相关
    func addTimer() {
        if contentViews.count > 1, timeInterval > 0, timer == nil {
            timer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(nextPage), userInfo: nil, repeats: true)
            guard let timer = timer else { return }
            RunLoop.current.add(timer, forMode: .common)
        }
    }
    func removeTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    @objc private func nextPage() {
        next()
    }
}

// MARK: - 滚动代理
extension CycleScrollView: UIScrollViewDelegate {
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        synchronization()
        delegate?.cycleScrollViewDidScroll?(cycleScrollView: self)
    }
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        removeTimer()
        delegate?.cycleScrollViewWillBeginDragging?(cycleScrollView: self)
    }
    
    public func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        delegate?.cycleScrollViewDidEndDragging?(cycleScrollView: self)
        addTimer()
    }
    
    public func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        delegate?.cycleScrollViewWillBeginDecelerating?(cycleScrollView: self)
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        delegate?.cycleScrollViewDidEndDecelerating?(cycleScrollView: self)
    }
}
